'use strict';

import React, {Component} from 'react';
import {dispatch} from '../core/TodoDispatcher';

const ENTER_KEY_CODE = 13;

export default class ListFooter extends Component {

    constructor(props) {
        super(props);
        this.state = {text: ''};

        this._onTextChange = this._onTextChange.bind(this);
        this._onAddClick = this._onAddClick.bind(this);
        this._onKeyDown = this._onKeyDown.bind(this);
        this._addTodo = this._addTodo.bind(this);
    }

    _onTextChange(e) {
        this.setState({text: e.target.value});
    }

    _onAddClick(e) {
       this._addTodo();
    }

    _onKeyDown(e) {
        if(e.keyCode === ENTER_KEY_CODE) {
            this._addTodo();
        }
    }

    _addTodo() {
        let text = this.state.text;
        this.props.addTodo(text);
        this.setState({text: ''});
    }

    render() {
        return (
            <div className="input-group">
                <input
                    onKeyDown={this._onKeyDown}
                    onChange={this._onTextChange}
                    type="text"
                    className="form-control"
                    placeholder="Task name..."
                    value={this.state.text}
                />

                <span className="input-group-btn">
                    <button
                        onClick={this._onAddClick}
                        className="btn btn-default"
                        type="button">
                        Add
                    </button>

                </span>
            </div>
            );
    }
}