'use strict';

import React, {Component} from 'react';
import classNames from 'classnames';
import {dispatch} from '../core/TodoDispatcher';

export default class TodoItem extends Component {
    constructor(props) {
        super(props);

        this._onClick = this._onClick.bind(this);
    }

    _onClick(e) {
        e.preventDefault();

        let actionType = this.props.complete ? 'todo/undo-complete' : 'todo/complete';

        dispatch({
            type: 'todo/toggle-complete',
            id: this.props.id,
            listId: this.props.listId,
        });
    }

    render() {
        let textStyle = {textDecoration: this.props.complete ? "line-through" : "none"};
        let aClass = classNames('todo-item', 'list-group-item', 'noselect', {'list-group-item-success': this.props.complete});
        
        return (
            <a
                onClick={this._onClick}
                className={aClass}>
                <span style={textStyle} >{this.props.text}</span>
            </a>
            );
    }
}