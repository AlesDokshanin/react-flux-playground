'use strict'

import React, {Component} from 'react';
import TodoStore from '../core/TodoStore';
import ListHeading from './ListHeading';
import ListMainSection from './ListMainSection';
import ListFooter from './ListFooter';
import {dispatch} from '../core/TodoDispatcher';


export default class TodoList extends Component {
    constructor(props) {
        super(props);

        this._addTodo = this._addTodo.bind(this);
        this._renameList = this._renameList.bind(this);
    }

    _addTodo(text) {    
        text = text.trim();
        dispatch({
            type: 'todo/create',
            listId: this.props.list.id,
            text,
        });
    }

    _renameList(newTitle) {
        newTitle = newTitle.trim();
        dispatch({
            type: 'todolist/rename',
            listId: this.props.list.id,
            newTitle: newTitle,
        });
    }

    render() {
        return (
            <div className="todolist col-sm-4 col-md-3">
                <ListHeading renameList={this._renameList} title={this.props.list.title} />
                <ListMainSection listId={this.props.list.id} todos={this.props.list.todos} />
                <ListFooter addTodo={this._addTodo} />
            </div>
            );
    }
}