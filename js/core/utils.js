'use strict'

export function generateId() {
    return Date.now() + Math.round(Math.random() * 1000);
}