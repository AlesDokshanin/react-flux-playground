'use strict'

import Immutable from 'immutable';
import {generateId} from './utils';

const TodoRecord = Immutable.Record({
    id: undefined,
    complete: undefined,
    text: undefined,
});


export default class TodoItem extends TodoRecord {
    id;
    complete;
    text;

    constructor(text) {
        super({
            id: generateId(),
            complete: false,
            text,
        });
    }
}
