'use strict'

import Immutable from 'immutable';
import {ReduceStore} from 'flux/utils';
import TodoItem from './Todo';
import TodoList from './TodoList';
import TodoDispatcher from './TodoDispatcher';

class TodoStore extends ReduceStore {
    getInitialState() {
        return Immutable.OrderedMap();
    }

    reduce(state, action) {
        switch(action.type) {
            case 'todo/create':
                return createTodo(state, action.text, action.listId);

            case 'todo/toggle-complete':
                return toggleCompleteTodo(state, action.listId, action.id);

            case 'todolist/create':
                return createTodoList(state);

            case 'todolist/rename':
                return renameTodoList(state, action.listId, action.newTitle);
           
            default: 
                return state;
        }
    }
}

function createTodo(state, text, listId) {
    if(!text) {
        return state;
    }

    let instance = new TodoItem(text);
    let oldTodos = state.get(listId).todos;
    let newTodos = oldTodos.set(instance.id, instance);

    return state.setIn([listId, 'todos'], newTodos);
}

function createTodoList(state) {
    let obj = new TodoList("New List");
    return state.set(obj.id, obj);
}

function toggleCompleteTodo(state, listId, todoId) {
    let oldValue = state.get(listId).todos.get(todoId).complete;
    return state.setIn([listId, 'todos', todoId, 'complete'], !oldValue); 
}

function renameTodoList(state, listId, newTitle) {
    return state.setIn([listId, 'title'], newTitle);
}


const instance = new TodoStore(TodoDispatcher);
export default instance;